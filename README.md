# HealthCheck

## Description

Discord bot to check status of service

## Usage

```bash
docker run -it -d \
  -e TOCHECK='{"0": {"proto": "TCP", "host": "127.0.0.1", "port": "80"}, "1": {"proto": "HTTP", "URL": "http://127.0.0.1"}}' \
  -e faviconURL="http://www.exemple.com/favicon.ico" \
  -e TOKEN="Discord bot token" \
  registry.gitlab.com/lokarde/healthcheck
```
