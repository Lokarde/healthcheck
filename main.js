const Discord = require('discord.js');
const client = new Discord.Client();
const http = require('http');
const https = require('https');
const net = require('net');

function check() {
  toCheck = JSON.parse(process.env.TOCHECK);
  var netclient = new net.Socket();

  netclient.on('error', function(err){
    work = false;
  })

  work = true
  for (tc in toCheck) {
    if (toCheck[tc].proto == 'TCP') {
      work = false;
      netclient.connect(toCheck[tc].port, toCheck[tc].host);
    } else if (toCheck[tc].proto == 'HTTP') {
      http.get(toCheck[tc].URL, (res) => {
        //console.log(res.statusCode);
        if (res.statusCode != 200) {
          client.user.setStatus('dnd');
          client.user.setActivity('les erreurs HTTP D:', {type: 'WATCHING'})
          work = false;
        } else {

        }
      }).on('error', (e) => {
        console.error(e);
      });
    } else if (toCheck[tc].proto == 'HTTPS') {
      https.get(toCheck[tc].URL, (res) => {
        //console.log(res.statusCode);
        if (res.statusCode != 200) {
          client.user.setStatus('dnd');
          client.user.setActivity('les erreurs HTTP D:', {type: 'WATCHING'})
          work = false;
        } else {

        }
      }).on('error', (e) => {
        console.error(e);
      });
    }
    netclient.end();
  }

  if (work) {
    client.user.setStatus('online');
    client.user.setActivity('le serveur qui MARCHE :DDD', {type: 'WATCHING'})
  }

}

client.on('ready', () => {
  if (process.env.faviconURL) {
    client.user.setAvatar(process.env.faviconURL)
      .catch(console.error);
  }

  setInterval(check,1000);

  console.log(`Logged in as ${client.user.tag}!`);
});

client.login(process.env.TOKEN);
